// JavaScript Synchronous vs Asynchronous

// Synchronous
// -- Statements are executed one at a time

console.log(`Hello, World!`)
// conosle.log(`Hello, Again!`) -- stops here cause of an error
console.log(`Goodbye!`)




// Asynchronous
// -- Statements can proceed to execute while other codes are running in the background

// Fetch API -- Allows to asynchronously request for a resource (data)
// Get all post

console.log(fetch('https://jsonplaceholder.typicode.com/posts') );



// Check the status of the request
/*	Syntax:
		fetch('URL')
		.then((response) => {} )
*/
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status) ); //200 -- means all good/clear



// Retrieve the contents/data from the "Response" object
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json() )
.then((json) => console.log(json) );



// Creating a function using "async" and "await" keywords
// "async" and "await" -- used to achieve an asynchronous state
async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);

	console.log(typeof result);

	let json = await result.json()

	console.log(json);
}

fetchData();



// Getting a specific post
// (retrieve, /post/:id, Get)
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json() )
.then((json) => console.log(json) );



// Creating a post
/*	Syntax:
		fetch('URL', options)
		.then((response) => {} )
*/
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the "request" objet to "POST"
	// Default method is GET
	method: 'POST',
	// Sets the header data of the "request" object to send to the backend
	header: {
		'Content-type': 'application/json'
	},
	// Sets the content/body data of the "request" object to send to the backend
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello, World!',
		userID: 1
	}),
})
.then((response) => response.json() )
.then((json) => console.log(json) );



// Update a post using the PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Post',
		body: 'Hello, again!',
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));



// Update a post using the PATCH method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Corrected Post',
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));



// Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});

// Filter the posts
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));